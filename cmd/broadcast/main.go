package main

import (
	"log"
	"math/rand"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/segmentio/ksuid"
	"gitlab.com/remotejob/gochatvuews/internal/config"
	"gitlab.com/remotejob/gochatvuews/internal/domains"
	"gitlab.com/remotejob/gochatvuews/internal/utils"
	"gitlab.com/remotejob/gochatvuews/pkg/getnewimgs"
)

var (
	conf   *config.Config
	images []domains.Image
	err    error
)

func init() {

	conf, err = config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}
	images, err = getnewimgs.Get(conf.Imageservice.Url, "500")
	if err != nil {

		log.Fatalln(err)
	}

}

func main() {

	for {

		url := "http://" + conf.Wsserver.Url + "/allclients"
		// log.Println(url)

		wsclients := utils.Wshttpget(url)

		log.Println("connected",len(wsclients))

		timeDuration60sec := 60 * time.Second

		for k, cl := range wsclients {

			diff := cl.Active.Sub(cl.Created)

			rand.Seed(time.Now().UnixNano())

			newimgid := rand.Intn(len(images))
			newimg := images[newimgid]
			newimg.Timeid = ksuid.New().String()

			newmsg := domains.Message{
				Id:       uuid.NewV4().String(),
				Type:     "join",
				Username: newimg.Name,
				Message:  "Join chatti",
				Img:      newimg,
			}

			if diff < timeDuration60sec {
				// log.Println("diff < 60")
				newmsg.Nguid = k
				url := "http://" + conf.Wsserver.Url + "/sendmsg"
				utils.Sendmsg(url, newmsg)

			} else {

				rand.Seed(time.Now().UnixNano())
				if rand.Intn(9) == 0 {
					log.Println("Lottery by 9")
					newmsg.Nguid = k

					url := "http://" + conf.Wsserver.Url + "/sendmsg"
					utils.Sendmsg(url, newmsg)

				}

			}

		}

		time.Sleep(7 * time.Second)

	}

}
