package main

import (
	"log"
	"net/http"
	"time"

	"github.com/adrianosela/certcache"
	"github.com/adrianosela/sslmgr"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/remotejob/gochatvuews/internal/config"
	handlerMsg "gitlab.com/remotejob/gochatvuews/pkg/handerMsg"
	"gitlab.com/remotejob/gochatvuews/pkg/handler"
	"golang.org/x/crypto/acme/autocert"
)

var (
	conf *config.Config
	err  error
)

func Routes(configuration *config.Config) *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/alive", handler.New(conf).Alive)
	router.HandleFunc("/allclients", handler.New(conf).Getallclients)
	router.HandleFunc("/sendmsg", handler.New(conf).Sendmsg)
	router.HandleFunc("/ws/{id}", handler.New(conf).HandleConnections)
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("/root/gochatvuewsfront/dist"))))

	return router
}
func init() {
	conf, err = config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}
}
func main() {

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, // All origins

	})

	router := Routes(conf)

	handlercors := c.Handler(router)

	go handlerMsg.Elab(conf)

	ss, err := sslmgr.NewServer(sslmgr.ServerConfig{
		Hostnames: []string{"ippayment.info", "localhost","infochat.fi","chatfi.fi","0pf.info"},
		HTTPPort:  ":80",
		HTTPSPort: ":443",
		Handler:   handlercors,
		// ServeSSLFunc: func() bool {
		// 	return strings.ToLower(os.Getenv("PROD")) == "true"
		// },
		CertCache: certcache.NewLayered(
			certcache.NewLogger(),
			autocert.DirCache("."),
		),
		ReadTimeout:         5 * time.Second,
		WriteTimeout:        5 * time.Second,
		IdleTimeout:         25 * time.Second,
		GracefulnessTimeout: 5 * time.Second,
		GracefulShutdownErrHandler: func(e error) {

			log.Fatal(e)
		},
	})

	if err != nil {
		log.Fatal(err)
	}

	ss.ListenAndServe()

}
