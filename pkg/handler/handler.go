package handler

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/remotejob/gochatvuews/internal/config"
	"gitlab.com/remotejob/gochatvuews/internal/domains"
	"gitlab.com/remotejob/gochatvuews/pkg/getanswer"
	// "gitlab.com/remotejob/gochatvuews/pkg/getnewimgs"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Alive(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Alive", "OK")

	w.Write([]byte("OK"))
}

func (config *Config) Sendmsg(w http.ResponseWriter, r *http.Request) {

	var msg domains.Message

	err := json.NewDecoder(r.Body).Decode(&msg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// log.Println(msg)
	client := config.Clients[msg.Nguid]
	client.Active = time.Now()
	config.Clients[msg.Nguid] = client
	err = config.Clients[msg.Nguid].Ws.WriteJSON(msg)
	if err != nil {
		log.Printf("handle Message error ID?: %v", err)
		log.Println("msg ?", msg)
		delete(config.Clients, msg.Nguid)

	}

}

func (config *Config) Getallclients(w http.ResponseWriter, r *http.Request) {

	resbyte, err := json.Marshal(config.Clients)
	if err != nil {

		log.Fatalln(err)
	}

	w.Header().Set("Content-Type", "application/json")

	w.Write(resbyte)

}

func (config *Config) HandleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	config.Hits++
	log.Println("Hits", config.Hits)
	vars := mux.Vars(r)
	nguid := vars["id"]
	var selectedImg domains.Image
	now := time.Now()

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("error %v", err)
		return
	}
	// Make sure we close the connection when the function returns
	defer ws.Close()

	selectedint := rand.Intn(len(config.Imgs))

	selectedImg = config.Imgs[selectedint]

	// log.Println(selectedImg)

	msg := domains.Message{
		Id:       uuid.NewV4().String(),
		Type:     "selected",
		Nguid:    nguid,
		Username: selectedImg.Name,
		Message:  "join chatti",
		Img:      selectedImg,
	}
	err = ws.WriteJSON(msg)
	if err != nil {
		log.Printf("handle Message error ID?: %v", err)
		log.Println("msg ?", msg)
		delete(config.Clients, msg.Nguid)

	}

	msg = domains.Message{
		Id:       uuid.NewV4().String(),
		Type:     "newmsg",
		Nguid:    nguid,
		Username: selectedImg.Name,
		Message:  config.Greetingslice[rand.Intn(len(config.Greetingslice))],
		Img:      selectedImg,
	}
	err = ws.WriteJSON(msg)
	if err != nil {
		log.Printf("handle Message error ID?: %v", err)
		log.Println("msg ?", msg)
		delete(config.Clients, msg.Nguid)

	}

	client := domains.Client{
		Created: now,
		Ws:      ws,
		Active:  now,
		Img:     selectedImg,
	}

	config.Clients[nguid] = client

	for {
		var msg domains.Message
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Printf("delete client error: %v", err)
			delete(config.Clients, nguid)
			break
		}
		u2 := uuid.NewV4()
		msg.Id = u2.String()
		config.Broadcast <- msg

		if msg.Type == "newmsg" && msg.Username == "mina" {

			payload, err := getanswer.GetAnswer(config.Mlservice.Url, msg)
			if err != nil {

				log.Fatalln(err)
			}

			log.Println("payload",payload)

			u2 = uuid.NewV4()
			msg.Id = u2.String()
			msg.Message = payload.Answer
			// msg.Username = payload.User
			// msg.Type = payload.Type

			config.Broadcast <- msg

		}

	}
}
