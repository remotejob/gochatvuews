package getanswer

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/remotejob/gochatvuews/internal/domains"
)

func GetAnswer(url string,clmsg domains.Message) (domains.Payload, error) {

	var retload domains.Payload

	m := map[string]interface{}{
		"Nguid":    clmsg.Nguid,
		"Chatuuid": clmsg.Chatuuid,
		"Message":  clmsg.Message,
		"Img": clmsg.Img,
	}

	bytesRepresentation, err := json.Marshal(m)
	if err != nil {

		return retload, err
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
	}

	json.NewDecoder(resp.Body).Decode(&retload)

	return retload, nil

}
