package handlerMsg

import (
	"log"

	"gitlab.com/remotejob/gochatvuews/internal/config"
)

func Elab(conf *config.Config) {

	for {
		msg := <-conf.Broadcast

		log.Println("brodcat", msg)
		err := conf.Clients[msg.Nguid].Ws.WriteJSON(msg)
		if err != nil {
			log.Printf("handle Message error ID?: %v", err)
			log.Println("msg ?", msg)
			delete(conf.Clients, msg.Nguid)

		}
	}
}
