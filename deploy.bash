
#scp wsserver.conf.toml root@64.225.9.26:gochatvuews
#scp data/hellophrase.csv root@64.225.9.26:gochatvuews/data

go build -o bin/wsserver gitlab.com/remotejob/gochatvuews/cmd/wsserver && \
ssh  root@64.225.9.26 systemctl stop httpsproxy.service && \
scp bin/wsserver root@64.225.9.26:gochatvuews && \
ssh  root@64.225.9.26 systemctl start httpsproxy.service


go build -o bin/broadcast gitlab.com/remotejob/gochatvuews/cmd/broadcast && \
ssh  root@64.225.9.26 systemctl stop broadcast.service && \
scp bin/broadcast root@64.225.9.26:gochatvuews/ && \
ssh  root@64.225.9.26 systemctl start broadcast.service


#scp wsserver.conf.toml root@165.227.101.53:gochatvuews
#scp data/hellophrase.csv root@165.227.101.53:gochatvuews/data

go build -o bin/wsserver gitlab.com/remotejob/gochatvuews/cmd/wsserver && \
ssh  root@165.227.101.53 systemctl stop httpsproxy.service && \
scp bin/wsserver root@165.227.101.53:gochatvuews && \
ssh  root@165.227.101.53 systemctl start httpsproxy.service


go build -o bin/broadcast gitlab.com/remotejob/gochatvuews/cmd/broadcast && \
ssh  root@165.227.101.53 systemctl stop broadcast.service && \
scp bin/broadcast root@165.227.101.53:gochatvuews/ && \
ssh  root@165.227.101.53 systemctl start broadcast.service