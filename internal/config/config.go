package config

import (
	"bufio"
	"encoding/csv"
	"log"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/remotejob/gochatvuews/internal/domains"
	"gitlab.com/remotejob/gochatvuews/pkg/getnewimgs"
)

type Constants struct {
	Imageservice struct {
		Url string
	}
	Mlservice struct {
		Url string
	}
	Wsserver struct {
		Url string
	}
	Greetings struct {
		Url string
	}
}

type Config struct {
	Constants
	Clients       map[string]domains.Client
	Broadcast     chan domains.Message // broadcast channel
	Imgs          []domains.Image
	Greetingslice []string
	Hits          int64
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}
	clients := make(map[string]domains.Client)
	config.Clients = clients
	broadcast := make(chan domains.Message) // broadcast channel
	config.Broadcast = broadcast
	imgs, err := getnewimgs.Get(config.Imageservice.Url, "6")
	if err != nil {

		log.Fatalln(err)
	}
	config.Imgs = imgs

	
	csvfile, err := os.Open(config.Constants.Greetings.Url)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}
	r := csv.NewReader(bufio.NewReader(csvfile))
	r.Comma = '\t'
	result, err := r.ReadAll()
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}
	for _,res :=range result {

		config.Greetingslice= append(config.Greetingslice,res[0])	

	}


	return &config, err
}
func initViper() (Constants, error) {
	viper.SetConfigName("wsserver.conf") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")             // Search the root directory for the configuration file
	err := viper.ReadInConfig()          // Find and read the config file
	if err != nil {                      // Handle errors reading the config file
		return Constants{}, err
	}

	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
