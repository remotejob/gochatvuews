package domains

import (
	"time"

	"github.com/gorilla/websocket"
)

type Client struct {
	Created time.Time
	Ws      *websocket.Conn `json:"-"`
	Active  time.Time
	Chatid  string
	Lastmsg string
	Img     Image
}


type Message struct {
	Id       string `json:"id"`
	Type     string `json:"type"`
	Nguid    string `json:"nguid"`
	Chatuuid string `json:"chatuuid"`
	Username string `json:"username"`
	Message  string `json:"message"`
	Img      Image
}

type Image struct {
	Timeid        string
	Id            int
	Name          string
	Age           int
	City          string
	Phone         string
	Img_file_name string
}
type Payload struct {
	Answer string `json:"Answer"`
	// Message string `json:"message"`
	// Type    string `json:"type"`
	// User    string `json:"user"`
}
