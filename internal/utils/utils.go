package utils

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/remotejob/gochatvuews/internal/domains"
)

func Sendmsg(url string, msg domains.Message) {
	bytesRepresentation, err := json.Marshal(msg)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = http.Post(url, "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
	}
}

func Wshttpget(url string) map[string]domains.Client {

	result := make(map[string]domains.Client)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalln(err)
	}

	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer r.Body.Close()

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	var objmap map[string]*json.RawMessage
	err = json.Unmarshal(bodyBytes, &objmap)
	if err != nil {
		log.Panicln(err)
	}

	for k, _ := range objmap {

		var cl domains.Client

		err = json.Unmarshal(*objmap[k], &cl)
		if err != nil {

			log.Panicln(err)
		}

		result[k] = cl

	}

	json.NewDecoder(r.Body).Decode(&result)

	return result

}
